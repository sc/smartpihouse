#!/bin/bash

# IP address of the Raspberry Pi
pi_ip="raspberrypi.local"

# SSH username for the Raspberry Pi
pi_username="simon"

# Path to store the captured images on the Raspberry Pi
pi_image_dir="/home/simon/camera_images"

# Path to store the captured images on the Debian server
server_image_dir="monitoring"

# Create the directories if they do not exist
ssh "$pi_username@$pi_ip" "mkdir -p $pi_image_dir"
mkdir -p "$server_image_dir"

while true; 
do
    # Get the current date and time on the server
    date=$(date +%Y-%m-%d_%H-%M-%S)
    # Capture an image using libcamera-still on the Raspberry Pi
    ssh "$pi_username@$pi_ip" "libcamera-still -o $pi_image_dir/image_$date.jpg"
    rm www/last.jpg
    ln -s "/home/simon/project/smartpihouse/monitoring/image_$date.jpg" "/home/simon/project/smartpihouse/www/last.jpg"
    # Transfer the captured image from the Raspberry Pi to the Debian server
    scp "$pi_username@$pi_ip:$pi_image_dir/image_$date.jpg" "$server_image_dir"
    ssh "$pi_username@$pi_ip" "rm $pi_image_dir/image_$date.jpg"
    # Add the date to the image as a watermark
    mean_intensity=$(convert "$server_image_dir/image_$date.jpg" -colorspace gray -format "%[fx:mean*255]" info:)
    threshold=18 # Adjust this value based on how dark you consider a frame to be black (0-255)
    echo $mean_intensity "vs" $threshold
    if (( $(echo "$mean_intensity > $threshold" | bc -l) )); then
        convert "$server_image_dir/image_$date.jpg" -pointsize 30 -fill white -undercolor '#00000080' -gravity SouthEast -annotate +10+10 "$date" "$server_image_dir/image_$date.jpg"
    # Wait for 30 seconds before capturing the next image
    else
        echo "dark age, dark souvenir, don't want to remember"
    fi 
    
    sleep 30
done
