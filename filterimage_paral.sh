#!/bin/bash

if [ $# -ne 2 ]; then
  echo "Usage: $0 <input_folder> <output_folder>"
  exit 1
fi

input_folder="$1"
output_folder="$2"
threshold=10 # Adjust this value based on how dark you consider a frame to be black (0-255)
last=`ls -f $output_folder | sed "s/-//g" | sed "s/_//g" | sed "s/image//g" | sed "s/jpg//g" | sed "s/\.//g" | sort -n | tail -n 1`
last=`basename $last`

# Create output folder if it doesn't exist
mkdir -p "$output_folder"

process_image() {
  img_file="$1"
  output_folder="$2"
  threshold="$3"
  last="$4"
  img_filename=$(basename "$img_file")
  output_file="${output_folder}/${img_filename}"
  # Check if the image has already been processed
  cur=`ls -f $img_file | sed "s/-//g" | sed "s/_//g" | sed "s/image//g" | sed "s/jpg//g" | sed "s/\.//g" | tail -n 1`
  echo $last "vs" $cur
  cur=`basename $cur`

  #if [ -f "$output_file" ]; then
  #    echo "skip $img_filename"
  #    return
  #fi

  if (( cur < last )); then
      echo "skip and delete $img_filename"
      rm $img_file
      return
  else  
      echo "using $cur"
  fi

  mean_intensity=$(convert "$img_file" -colorspace gray -format "%[fx:mean*255]" info:)

  if (( $(echo "$mean_intensity > $threshold" | bc -l) )); then
    cp "$img_file" "$output_folder/"
  fi
}
export -f process_image

find "$input_folder" -type f \( -iname "*.jpg" -o -iname "*.png" \) | parallel -j 10 process_image {} "$output_folder" "$threshold" "$last"


