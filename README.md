Dependences:
libmariadb-dev 


R package¨

Installer mysql sur le master :

```bash
sudo apt install mariadb-server
```

Configuration:

```bash
sudo mysql -uroot
```


```SQL
GRANT ALL PRIVILEGES on *.* 
  TO 'root'@'%' 
  IDENTIFIED BY 'qu>EM&4B]LWGe-+kbtR,IOqGL' with GRANT OPTION;

FLUSH PRIVILEGES;
```

Modify configuration:


 sudo vim /etc/mysql/mariadb.conf.d/50-server.cnf


Comment: 

bind-address            = 127.0.0.1

sudo systemctl restart mysql


this should work:

```bash
mysql -uroot -h$HOSTNAME.local -p
```


use mysql;

```SQL
CREATE USER 'zeus'@'%'
  IDENTIFIED BY 'Fx75wMb0GX83vlaAZpp2ta18R';

CREATE DATABASE gaïa;

GRANT ALL PRIVILEGES  
  ON gaïa.*
  TO 'zeus'@'%' ;

FLUSH PRIVILEGES;

use gaïa
CREATE TABLE temperature(
    host VARCHAR(40) NOT NULL,
    date DATETIME(0) NOT NULL,
    local_id INT,
    w1_id VARCHAR(100) NOT NULL,
    temp VARCHAR(44) NOT NULL
);

```

Attention problem avec mariadb, la derniere version ne prend pas en compte l'utf8

Install mysql-connector-python abd W1ThermSensor

python3 -m pip install mysql-connector-python
python3 -m pip install W1ThermSensor 


if pip isn't available: 
apt install python3-pip

Source:
https://medium.com/better-programming/how-to-install-mysql-on-a-raspberry-pi-ad3f69b4a094


Install it as a daemon:

```bash
sudo bash install.sh
```

Be Carefull if the user pi has changed/doesn't exist, this won't load. I should install python modul in the system not just for specific users.
This is even more true now that Raspbian disable pi user by default!
Et mettre des sudo partou ne marche pas...

TODO:
passer par systemd ça a l'aire bien plus simple pour tout gerer.:
https://unix.stackexchange.com/questions/166473/debian-how-to-run-a-script-on-startup-as-soon-as-there-is-an-internet-connecti

en tout ca pour gererer le serveur shiny: ça permettre de pourvoir redemmarerle serveur sans redemarrer la pie!!

